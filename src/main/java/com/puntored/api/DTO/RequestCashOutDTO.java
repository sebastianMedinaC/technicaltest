package com.puntored.api.DTO;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import lombok.Data;

@Data
public class RequestCashOutDTO {
	
	@NotNull
	@Positive
	private int amount; 

}
