package com.puntored.api.reposiroty;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.puntored.api.entity.UsersEntity;

@Repository
public interface InventoryBillsRepository extends CrudRepository<UsersEntity, String> {
	
	public UsersEntity findByName(String name);

}
