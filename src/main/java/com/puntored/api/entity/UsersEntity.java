package com.puntored.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "USERS", schema = "public")
@Data
public class UsersEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -216449681855337442L;

	@Column(name = "name")
	@Id
	private String name;

	@Column(name = "balance")
	private Integer balance;

}
