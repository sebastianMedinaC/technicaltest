package com.puntored.api.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "INTENTORY_BILLS", schema = "public")
@Data
public class Intentory_Bills implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1814384471715388964L;

	@Column(name = "denomination")
	@Id
	private Integer denomination;

	@Column(name = "amount")
	private Integer amount;

}
