package com.puntored.technicalTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.puntored.api.DTO.RequestCashOutDTO;
import com.puntored.api.entity.UsersEntity;
import com.puntored.api.reposiroty.UsersRepository;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@RestController
@Log4j2
public class TechnicalTestApplication {
	

	@Autowired
	private UsersRepository usersRepository;

	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestApplication.class, args);
	}

	@GetMapping("/")
	public String index() {
		return "Endpoints Disponibles\n" + "/cashout\n" + "/stats";
	}

	@PostMapping(value = "/cashout", consumes = { "application/json" }, produces = { "application/json" })
	public ResponseEntity<String> cashout(@RequestBody RequestCashOutDTO request) {

		boolean isNumeric = String.valueOf(request.getAmount()).chars().allMatch(Character::isDigit);
		if (!isNumeric) {
			return new ResponseEntity<>("Campo Amount no es númerico", HttpStatus.BAD_REQUEST);
		}

		UsersEntity dataUser = usersRepository.findByName("Pepito");//		
		log.info(dataUser.getBalance());		

		return new ResponseEntity<>(String.valueOf(request.getAmount()), HttpStatus.OK);
	}

	@GetMapping("/stats")
	public String stats() {
		return "Endpoints debuelve transaccione";
	}

}
