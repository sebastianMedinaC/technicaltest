 
# Notas:

La idea era conectarse desde un API de springBoot a una BD de postgresql donde estarian las tablas de:

- Usuarios con el nombre y el saldo
- Inventario de Billetes donde estaria la denominacion del billete  y su cantidad
- Una tabla de registro de las transaccion con la fecha, usuario, monto a retirar y estado de la transacción

Pero no pude conectarme a la BD 

LA URL BASE para el API es:

*localhost:8082/api/*

Comando para iniciar BD de postgresql en dokcer con las credenciales del archivo properties.env del API en SpringBoot:

`docker run -p 127.0.0.1:5432:5432 --name postgresDB -e POSTGRES_PASSWORD=user123 -e POSTGRES_USER=user123 -e POSTGRES_DB=cashMachine -d postgres:12-alpine`